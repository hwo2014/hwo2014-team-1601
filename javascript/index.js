var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
    onCarPositions(data);
    send({
      msgType: "throttle",
      data: 0.5
    });
  } else {
    if (data.msgType === 'join') {
      console.log('Joined');
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } 

    send({
      msgType: "ping",
      data: {}
    });
  }
});
var onCarPositions =  function(data){
  console.log(data);
  console.log(data.data[0].id);
  console.log(data.data[0].piecePosition);
}

var getSpeed = function() {
	var currentPiece = pieces[myCurrentCarPosition.piecePosition.pieceIndex];
	var previousPiece = pieces[myCurrentCarPosition.piecePosition.pieceIndex - 1];

	if (myCurrentCarPosition.piecePosition.pieceIndex === myPrevCarPosition.piecePosition.pieceIndex) {
		//TODO: Tick compensation? - Assumes that we get every tick. 
		//Could there be tick skipping or data flow with every 2nd tick reach destination.
		return myPrevCarPosition.piecePosition.inPieceDistance - myCurrentCarPosition.piecePosition.inPieceDistance;
	} else {
		//TODO: Assumes that in a tick cant skip pieces.
		lengthToPreviusPieceEnd = previousPiece.length - myPrevCarPosition.piecePosition;
		return lengthToPreviusPieceEnd + myCurrentCarPosition.piecePosition.inPieceDistance;
	}
}
jsonStream.on('error', function() {
  return console.log("disconnected");
});
